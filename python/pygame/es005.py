import pygame 
pygame.init() 

screen = pygame.display.set_mode((500,500)) 
pygame.display.set_caption("es005")
purple = (102, 0, 102) 

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    pygame.draw.polygon(screen, purple, 
                    ((146, 0), (291, 106),(236, 277), (56, 277), (0, 106)))
    pygame.display.update() 
    