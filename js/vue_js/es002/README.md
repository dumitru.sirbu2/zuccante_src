# es002

Il nostro secondo esempio si articola: una lista di memo. Qui sotto alcuni riferimenti alla guida introduttiva.

## la direttiva `v-for`

Il riferimento per la documentazione  è [qui](https://v3.vuejs.org/guide/list.html#mapping-an-array-to-elements-with-v-for). Facciamo conoscenza con la *directive* `v-for`, per le API [qui](https://v3.vuejs.org/api/directives.html#v-for) usato per fare il *rendering* di una lista (i suoi elementi) o di un oggetto (le sue proprietà). È interessante l'uso dell'attributo speciale `:key` che ci permette di modificare l'ordine in cui gli elementi vengono visualizzati (utile per mettere in primo piano oggetti più importanti)
```
<div v-for="item in items" :key="item.id">
  {{ item.text }}
</div>
```

## form imput e `v-model`

[Qui](https://v3.vuejs.org/guide/forms.html) abbiamo parte della guida iniziale da leggere. Per le API [qui](https://v3.vuejs.org/guide/forms.html).


## *conditional rendering* 

Nella guida [qui](https://v3.vuejs.org/guide/conditional.html). Per la direttiva `v-if` le API [qui](https://v3.vuejs.org/guide/forms.html), invitiamo anche a leggere le successive.

## gli eventi

Già nel primo esempio abbiamo presentato *event handling* [qui](https://v3.vuejs.org/guide/events.html) con `v-on`.