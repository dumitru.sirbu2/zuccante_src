# es004

Un po' di esempi a completare gli *input* e sulle *computed properties*, queste a differenza dei metodi vengono calcolate in modo *recative* (ad eventi) aò cambiamento delle *property* da cui dipensono.
- "Form Input Bindings" [qui](https://v3.vuejs.org/guide/forms.html).
- "Computed Properties and Watchers" [qui](https://v3.vuejs.org/guide/computed.html#computed-properties).

In questo esempio ci siamo occupati delle ***computed properties***, in Vue esistono anche ***watcher*** ed entrambi sono *reactive*: il senso delle prime è quello di ottenere nuovi dati, *property*, a partire da dati già esistenti, altre *property*, in modo *reactive*, i *watcher* invece reagiscono al cambiamento di una proprietà, orientate quindi al cambiamento, ad esempio l'editing di un testo (come nel tutorial). 
> This is most useful when you want to perform asynchronous or expensive operations in response to changing data.

La sequenza in cui valutare cosa usare è la seguente  
```
method -> computed property -> watcher
``` 
Con `Axios` riprenderemo i *watcher*.

## un esempio di watcher

Prendiamo l'esempio della documentazione
```html
<div id="watch-example">
  <p>
    Ask a yes/no question:
    <input v-model="question" />
  </p>
  <p>{{ answer }}</p>
</div>
```
qui di seguito parte del sorgente
```js
// whenever question changes, this function will run
question(newQuestion, oldQuestion) {
    if (newQuestion.indexOf('?') > -1) {
        this.getAnswer()
    }
}
```

## materiali 

[1] "Watchers vs Computed Properties" di Flavio Copes [qui](https://flaviocopes.com/vue-methods-watchers-computed-properties/).
