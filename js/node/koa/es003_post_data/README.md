# routing and body

In questo esempio usaimo due *middleware*: `boa-body` [qui](https://github.com/dlau/koa-body) e `koa-router` [qui](https://github.com/ZijianHe/koa-router#module_koa-router--Router+allowedMethods). Abbiamo testato con
```
curl -H "Content-Type: application/json" -d '{"frutto" : "mela"}' localhost:3000/form
curl -d 'frutto = mela' localhost:3000/form
curl localhost:3000/form
```
ottenendo 
```
Request Body: {"frutto":"mela"}
Request Body: {"frutto ":" mela"}
form
```
il primo è *urlencoded* il secondo è *json*. Veniamo quindi al codice; le prime righe son le solite segue poi
``` javascript
app
  .use(koaBody())  
  .use(router.routes());
  //.use(router.allowedMethods());
```
delle tre la più oscura può risultare l'ultima, nelle api troviamo
> Returns separate middleware for responding to `OPTIONS` requests with an `Allow` header containing the allowed methods, as well as responding with `405 Method Not Allowed` and `501 Not Implemented` as appropriate.

Qui sotto riportiamo l'esempio che trpviamo nelle API
``` javascript
var Koa = require('koa');
var Router = require('koa-router');
var Boom = require('boom');

var app = new Koa();
var router = new Router();

app.use(router.routes());
app.use(router.allowedMethods({
  throw: true,
  notImplemented: () => new Boom.notImplemented(),
  methodNotAllowed: () => new Boom.methodNotAllowed()
}));
```
> **`router.allowedMethods([options]) ⇒ function`**

- `[options]` 	Object 	
- `[options.throw]` 	Boolean 	throw error instead of setting status and header
- `[options.notImplemented]` 	function 	throw the returned value in place of the default NotImplemented error
- `[options.methodNotAllowed]` 	function 	throw the returned value in place of the default MethodNotAllowed error