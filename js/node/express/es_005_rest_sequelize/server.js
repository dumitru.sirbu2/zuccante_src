const express = require("express");
const app = express();
const Sequelize = require('sequelize');

app.use(express.json());

// ***** DB *****

// db connection
let sequelize = new Sequelize('sequelize', 'sequelize', 'sequelize@2019', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
      max: 5, 
      min: 0, 
      idle: 10000 
    },
});

// sync models
const Memo = sequelize.import(__dirname + '/memo');
Memo.sync();

// ***** END DB *****

// **** REST ******

// GET all
app.get('/', (req, res) => {  
    Memo.findAll().then(memos => {
        res.json(memos);
    })
});

// GET only one using param
app.get('/:title', (req, res) => {
    let title = req.params.title;
    Memo.findOne({ where: {title: title}}).then(memo => {
        res.json(memo);
    })
});

// POST: insert
app.post('/', (req, res) => {
    console.log(`POST object: ${req.body.title}`);
    let title = req.body.title;
    let description = req.body.description;
    Memo.create({ title: title, description: description}).then(memo => {
        res.json({ msg : `memo: ${memo} created`});
    })
});

// PUT: update
app.put('/:title', (req, res) => {
    let title = req.params.title;
    let description = req.body.description;
    Memo.update({
        description: description
    },
    {
    where: {
        title: title
    }
    }).then(
        res.json({ msg : 'Memo updated'})
    )
});

// DELETE: delete
app.delete("/:title", (req, res) => {
    let title = req.params.title;
    Memo.destroy({
    where: {
            title: title
    }
    }).then(
        res.json({ msg : 'Memo deleted'})
    )   
});

// *** END REST ****

// starting server
app.listen(3000, () => console.log('Server ready'))

