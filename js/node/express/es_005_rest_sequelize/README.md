
# es_005_rest_sequelize

Questa è la nostra prima applicazione REST che fa uso di [sequelize](https://sequelize.org/v5/index.html). Un applicazione REST possiede stato ovvero memoria ed associa alle azioni del protocollo HTTP - meglio è parlare di HTTP REST - altre azioni in una base di dati (qui relazionale: MariaDB), vedi [qui](https://en.wikipedia.org/wiki/Representational_state_transfer).

## Il DB

Utilizzando l'ORM sequelize inizializziamo la connessione al DB precedentemente creato.

``` javascript
let sequelize = new Sequelize('sequelize', 'sequelize', 'sequelize@2019', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
      max: 5, // maximum number of connection in pool
      min: 0, // min ...
      idle: 10000 // the maximum time, in milliseconds, that a connection can be idle before being released
    },
});
```
Il *model*, vedi anche [qui](https://sequelize.org/v5/manual/models-definition.html) la documentazione, per ordinare il tutto lo abbiamo definito su di un file a parte
``` javascript
module.exports = (sequelize, DataTypes) => {
    const Memo = sequelize.define('memo', {
        title: {type: DataTypes.STRING(20), unique: true},
        description: DataTypes.TEXT
      },
      {
        freezeTableName: true,
      }
    );
    return Memo;
}
```
per poi importarlo in fase di avvio del server
``` javascript
const Memo = sequelize.import(__dirname + '/memo');
Memo.sync();
```
e sincronizzare il tutto con il DB, vedi [qui](https://sequelize.org/v5/manual/models-definition.html#import). Per la scrittura di un *model*  abbiamo a disposizione i seguenti [tipi](https://sequelize.org/v5/manual/data-types.html) mappati sui tipi SQL del DVNS prescelto. Per `define(...)` rimandiamo alle [qpi](https://sequelize.org/master/class/lib/sequelize.js~Sequelize.html#instance-method-define) ma anche a [qui](https://sequelize.org/v5/manual/getting-started.html).

## REST

Qui bisogna fare un attimo attenzione alla programmazione asincrona soppesando queli siano i valori realmente a disposizione.
``` javascript 
app.get('/:title', (req, res) => {
    let title = req.params.title;
    Memo.findOne({ where: {title: title}}).then(memo => {
        res.json(memo);
    })
});

```
funziona, mentre **non va**:
``` javascipt
app.get('/:title', (req, res) => {
    Memo.findOne({ where: {title: req.params.title}}).then(memo => {
        res.json(memo);
    })
});
```
Per il *queryng* vedi [qui](https://sequelize.org/v5/manual/querying.html). Per l'aggiornamento, l'inserimento e la cancellazione dobbiamo studiare [qui](https://sequelize.org/v5/manual/instances.html) facendo attenzione alla differenza che esiste fra **persistent** - istanze degli oggetti del *model* creati e salvati nel DB - e **non persistent instance** - istanze del *model* non salvate nel DB.
 Qui di seguito il REST completo

### GET

Tutti e poi un solo alemento:

 ``` javascript
 // GET all
app.get('/', (req, res) => {  
    Memo.findAll().then(memos => {
        res.json(memos);
    })
});

// GET only one using param
app.get('/:title', (req, res) => {
    let title = req.params.title;
    Memo.findOne({ where: {title: title}}).then(memo => {
        res.json(memo);
    })
});
```

### POST

Per inserire un nuovo elemento.
``` javascript
// POST: insert
app.post('/', (req, res) => {
    console.log(`POST object: ${req.body.title}`);
    let title = req.body.title;
    let description = req.body.description;
    Memo.create({ title: title, description: description}).then(memo => {
        res.json({ msg : `memo: ${memo} created`});
    })
});
```

### PUT

Per procedere all'agiornamento
``` javascript
// PUT: update
app.put('/:title', (req, res) => {
    let title = req.params.title;
    let description = req.body.description;
    Memo.update({
        description: description
    },
    {
    where: {
        title: title
    }
    }).then(
        res.json({ msg : 'Memo updated'})
    )
});
```

### DELETE

Per cancellare.
``` javascript
// DELETE: delete
app.delete("/:title", (req, res) => {
    let title = req.params.title;
    Memo.destroy({
    where: {
            title: title
    }
    }).then(
        res.json({ msg : 'Memo deleted'})
    )   
});
```
