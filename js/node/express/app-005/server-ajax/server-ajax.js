window.onload = function() {
  let form = document.getElementById('my-form');
  form.addEventListener("submit", (event) => {
    event.preventDefault();  // in order to send JSON string
    let jo = {};
    jo.name = form.elements['name'].value;
    jo.kind = form.elements['kind'].value;
    jo.shiny = form.elements['shiny'].value;
    console.log("Object from FORM: " + JSON.stringify(jo));
    
    
    // test the request states
    let request = new XMLHttpRequest();
    request.onreadystatechange = function(){
      if (request.readyState === 0) console.log("UNSENT");
      if (request.readyState === 1) console.log("OPENED");
      if (request.readyState === 2) console.log("HEADER RECEIVD");
      if (request.readyState === 3) console.log("LOADING");
      if (request.readyState === 4) {
        console.log("DONE")
        if (request.status === 200){
          console.log("server status 200, sent body: " + JSON.stringify(request.responseText));    
        } else {
          console.log("server bad responce");
        }
      }
    }
    // starting Ajax async
    request.open('POST', 'http://127.0.0.1:3000', true);
    // request.withCredentials = true;
    request.setRequestHeader('Content-type', 'application/json'); 
    request.send(JSON.stringify(jo));
  })
}
    
