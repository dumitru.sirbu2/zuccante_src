# CORS con Express

Per completare questa prima serie di esercitazioni, prima di connetterci ai DB relazionali tipo MariaDB o ad oggetti tipo MongoDB, affrontiamo qui una richuesta AJAX cros server. Useremo il srver node `http-server`, [qui](https://www.npmjs.com/package/http-server) l'home page, installato globalmente per far girare il `server-ajax` preparato sull'omonima cartella. 

## CORS

Per un utile ripasso rimandiamo al [documento](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS) di MDN. Uno Oggetto XHR (Ajax), o usando l'API `fetch`, garantisce non solo la possibilità di lavorare in modo **asincrono** ma anche è soggetto, nel suo utilizzo per comunicare in modo cross server, a vincoli: all'oggetto Ajax devono essere garantiti tutta una serie di permessi.

## CORS per Express

A differenza di un server NODE puro Express, come framework, offre maggior agilità nella gestione CORS; per avere un idea proponiamo l'articolo su Medium di A.Hevia: [qui](https://medium.com/@alexishevia/using-cors-in-express-cac7e29b005b). Per procedere col CORS abbiamo bisogno di installare il middleware per cors, vedi [qui](https://www.npmjs.com/package/cors)
```
npm install cors
```
Ora, nel nostro caso, vogliamo tentare una richiesta **preflight**, `POST` con json (vedi documentazione MDN). Nella documentazione del middleware `cors` troviamo il seguente esempio
```
var express = require('express')
var cors = require('cors')
var app = express()

app.options('/products/:id', cors()) // enable pre-flight request for DELETE request
app.del('/products/:id', cors(), function (req, res, next) {
  res.json({msg: 'This is CORS-enabled for all origins!'})
})

app.listen(80, function () {
  console.log('CORS-enabled web server listening on port 80')
})
```
nel nostro caso avremo un
```
app.post('/products/:id', cors() ....
```
in realtà, provando si scopre che funziona ??? anche
```
app.post('/products/:id',  ....
```
comunque la soluzione ufficiale (in riferimento alla documentazione) appare commentata nel sorgente!