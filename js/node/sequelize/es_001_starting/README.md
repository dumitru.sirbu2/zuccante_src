# First step with sequelize

Per le **api** [qui](https://sequelize.org/v5/identifiers).

## definiamo la connessione

Dopo aver letto un po' [qui](https://sequelize.org/v5/class/lib/sequelize.js~Sequelize.html#instance-constructor-constructor)
possiamo semplicemente creare la connessione con
``` javascript
const sequelize = new Sequelize('database', 'user', 'password', {
  dialect: 'mariadb'
})
```
nel nostro codice abbiamo aggiunto i parametri per il *pool*

``` javascript
let sequelize = new Sequelize('sequelize', 'sequelize', 'sequelize@2019', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
      max: 5, // dimensioni el pool
      min: 0, 
      idle: 10000 // resta in attesa al più per 10 s.
    },
});
```
Per MariaDB come MySQL lavorando con un *pool* possiamo rendere disponibili un certo numero di connessioni aperte perché siano usate in modo concorrente: una connessione libera non viene chiusa ma può essere riutilizzata (ogni connessione lavora in modo bloccante).

## model

Con un *model* definiamo l'oggetto che verrà salvato in modo persistente. Nella tabella corispondente, creata in automatico, verrà aggiunta `id` chiave primaria e i *timestamps*
``` sql
MariaDB [sequelize]> show create table user;
...
| user  | CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 |
...
1 row in set (0.00 sec)
```
Scriviamo
``` javascript
let User = sequelize.define('user', {
  firstName: {
    type: Sequelize.STRING,
    field: 'first_name' 
  },
  lastName: {
    type: Sequelize.STRING
  }},
  { freezeTableName: true }
);
```
`field: 'first_name'` ci permette di rinominare il campo nella tabella, `freezeTableName: true` non chiama al plurale la tabella nel nostro db: resta `user` e non diventa `users`. 

## connessione, inserimento e quert

La programmazione asincrona è delicata! Invitiamo ad un ripasso dei `Promise` [qui](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises).
``` Javascript
User.sync({force: true}).then(() => {
  return User.create({
    firstName: 'John',
    lastName: 'Hancock'
  });
}).then(() => {
  User.findAll().then(users => {
    console.log("All users:", JSON.stringify(users, null, 4));
  });
});
```
ovvero
``` javascript
User.sync({force: true}).then(
  // a connessione avvenuta faccio
}).then(() => {
  // ... quindi se tutto fila liscio faccio
});
```
Nel nostro caso, per pure ragioni didattiche
``` javascript
User.sync({force: true})
```
ricrea la tabella ad ogni esecuzione (cancellando eventualmente i dati salvati nel db in `user`). In JS (console di Node)
``` javascript
> console.log(JSON.stringify({ x: 5, y: 6 }));
{"x":5,"y":6}
undefined
> 
```
ma abbiamo ulteriori possibilità, vedi [questo](https://medium.com/javascript-in-plain-english/5-secret-features-of-json-stringify-c699340f9f27) articolo di Node.
```javascript
JSON.stringify(
    users, // array
    null, // function that selects the array elements
    4, // how much
));
```
