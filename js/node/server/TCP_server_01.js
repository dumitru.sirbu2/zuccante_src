var net = require('net');

// listen using tcat 127.0.0.1 3000

const server = net.createServer((socket) => {
    socket.end('goodbye\n');
}).on('error', (err) => {
    // Handle errors here.
    throw err;
});

server.listen(3000, () => {
    console.log('opened server on', server.address());
});