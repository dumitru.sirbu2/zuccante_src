async function getNumber(n) {
  return n;
}
/*
async function getNumber(n) {
  return Promise.resolve(n);
}
*/
let promise = getNumber(666);
promise.then((value) => console.log(value)); 