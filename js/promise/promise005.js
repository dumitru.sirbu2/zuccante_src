let momsPromise = new Promise((resolve, reject) => {
  momsSavings = 20000;
  priceOfPhone = 60000;
  if (momsSavings > priceOfPhone) {
    resolve({
      brand: "iphone",
      model: "13"
    });
  } else {
    reject("We donot have enough savings. Let us save some more money.");
  }
});

momsPromise.catch((reason) => {
  console.log("Mom coudn't buy me the phone because ", reason);
});

momsPromise.then((value) => {
  console.log("Hurray I got this phone as a gift ", JSON.stringify(value));
});

// try to comment
momsPromise.finally(() => {
  console.log(
    "Irrespecitve of whether my mom can buy me a phone or not, I still love her"
  );
});

/*

momsPromise.then(
  function(value) {
    console.log("Hurray I got this phone as a gift ", JSON.stringify(value));
  },
  function(reason) {
    console.log("Mom coudn't buy me the phone because ", reason);
  }
);

*/