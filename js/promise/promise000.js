const assert = require('assert');

Promise.resolve(123)
    .then(x => {
        assert.equal(x, 123);
    });

const myError = new Error('My error!');
Promise.reject(myError)
    .catch(err => {
        assert.equal(err, myError);
    });