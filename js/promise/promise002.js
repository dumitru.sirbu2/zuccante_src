const assert = require('assert');

function addAsync(x, y) {
    return new Promise(
        (resolve, reject) => {
            if (x === undefined || y === undefined) {
                reject(new Error('Must provide two parameters'));
            } else {
                resolve(x + y);
            }
        });
}

let x; // undefined
let y; // undefined

setTimeout(() => {
    console.log('x is assgned');
    x = 4;
}, 4 * 1000);

setTimeout(() => {
    console.log('y is assgned');
    y = 3;
}, 6 * 1000);

setTimeout(() => {
    addAsync(x, y)
    .then((value) => console.log(`OK: ${value}`))
    .catch((error) => console.log(error.message));
}, 7 * 1000);

