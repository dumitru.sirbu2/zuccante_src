// Promise { <pending> } .... 

promise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve({
      message: "The man likes to keep his word",
      code: "aManKeepsHisWord"
    });
  }, 10 * 1000);
});
console.log(promise);

// let change timeout below
setTimeout(() => {
  console.log(promise);
} , 10 * 1000);