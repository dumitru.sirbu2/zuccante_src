window.onload = function() {

  let cy = cytoscape({

  container: document.getElementById('cy'), // container to render in
  // container: $('#cy'),

  elements: [ 
    { 
      data: { id: 'a' }
    },
    { 
      data: { id: 'b' }
    },
    { 
      data: { id: 'c' }
    },
    { 
      data: { id: 'd' }
    },
    { 
      data: { id: 'e' }
    },
    { 
      data: { id: 'f' }
    },
    { 
      data: { id: 'ab', source: 'a', target: 'b' }
    },
    { 
      data: { id: 'ac', source: 'a', target: 'c' }
    },
    { 
      data: { id: 'cb', source: 'c', target: 'b' }
    },
    { 
      data: { id: 'bd', source: 'b', target: 'd' }
    },
    { 
      data: { id: 'ce', source: 'c', target: 'e' }
    },
    { 
      data: { id: 'de', source: 'd', target: 'e' }
    },
    { 
      data: { id: 'df', source: 'd', target: 'f' }
    },
     { 
      data: { id: 'ef', source: 'e', target: 'f' }
    }
  ],

  style: [ // the stylesheet for the graph
    {
      selector: 'node',
      style: {
        'background-color': '#666',
        'label': 'data(id)'
      }
    },

    {
      selector: 'edge',
      style: {
        'width': 2,
        'line-color': '#ccc',
        'curve-style': 'bezier', // .. and target arrow works properly
        'target-arrow-color': '#ccc',
        'target-arrow-shape': 'triangle'
      }
    }
  ],

  // see http://js.cytoscape.org/#layouts
  layout: {
    name: 'grid',
    rows: 2,
    cols: 4
  }
  });

  

  document.getElementById("btn1").addEventListener("click", function(){
    cy.elements('node#a').style({'background-color': '#f00'});
  });
  document.getElementById("btn2").addEventListener("click", function(){
    let options = { name: 'grid', rows: 3, clos: 4, animate: true};
    let layout = cy.layout(options);
    layout.run();
  });
  document.getElementById("btn3").addEventListener("click", function(){
    let options = { name: 'grid', rows: 1, clos: 3, animate: true};
    let layout = cy.layout(options)
    layout.run();
  });
  document.getElementById("btn4").addEventListener("click", function(){
    let options = { name: 'random', animate: true};
    let layout = cy.layout(options)
    layout.run();
  });
  let node;
  document.getElementById("btn5").addEventListener("click", function(){
    node = cy.add({
    group: 'nodes',
    data: { id: 'g' },
    position: { x: 200, y: 200 }
    });
  });
  document.getElementById("btn6").addEventListener("click", function(){
    if(node == null) return;
    cy.add([{
    group: 'edges',
    data: { id: 'ag', source: 'a', target: 'g'}
    },
    {
    group: 'edges',
    data: { id: 'fg', source: 'f', target: 'g'}
    }
    ]);
  });
}