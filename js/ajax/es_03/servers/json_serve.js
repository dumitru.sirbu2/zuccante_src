const http = require('http');

/* 
curl localhost:1337 -d "{}"
-> object
curl localhost:1337 -d "\"foo\""
-> string
curl localhost:1337 -d "not json"
-> error: Unexpected token o in JSON at position 1
*/

const server = http.createServer((req, res) => {

  let body = '';
  req.setEncoding('utf8');

  req.on('data', (chunk) => {
    body += chunk;
  });

  // The 'end' event indicates that the entire body has been received
  req.on('end', () => {
    try {
      const data = JSON.parse(body);
      // Write back something interesting to the user:
      res.write(typeof data);
      res.end();
    } catch (er) {
      // uh oh! bad json!
      res.statusCode = 400;
      return res.end(`error: ${er.message}`);
    }
  });
});

server.listen(1337);


