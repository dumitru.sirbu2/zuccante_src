# il server node

Qui affrontiamo la tematica "come inviare ad un *server* i dati tramite *form*", utile per un ripasso del TAG `<FORM>` imparare a crearne uno, [qui](https://developer.mozilla.org/en-US/docs/Learn/Forms/Your_first_form) una guida MDN e dare uno sguardo, smpre MDN, [qui](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/form).

## servers

Qui una proposta di *server* per riprendere la questione. Si invita ad un ripasso degli appunti Node su GitLab. `http.ClientRequest` è uno *stream*, vedi [qui](https://nodejs.org/api/http.html#class-httpclientrequest), per ulteriori dettagli sul metodo `end(...)` [qui](https://nodejs.org/api/http.html#requestenddata-encoding-callback).

## fallace

In questa cartella abbiamo un server ed un client che non dialogano, chiaramente la colpa è del client!
Nel client la porta `3000` viene data in quanto il *server* gira lì, ma con altri server, vedi estensione Live Server di VSC, è possibile cambiare il valore di tale porta.


## working

Questo è l'esempio completo, il nostro punto di arrivo. Abbiamo due server che dialogano via CORS. In `app.js` usiamo `preventDEfault()`, vedi [qui](https://developer.mozilla.org/en-US/docs/Web/API/Event/preventDefault) per bloccare il comportamento di default del submit. Ajax richiede una CORS, su MDN [qui](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS).

