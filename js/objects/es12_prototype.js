function doSomething(){}
/* the same as
class doSomething{
    constructor(){}
}
*/
console.log( doSomething.prototype );
console.log( doSomething.__proto__ );
console.log( doSomething.__proto__ === doSomething.prototype );


// an arrow faction is not the same as an usual function
const doSomethingFromArrowFunction = () => {};
console.log( doSomethingFromArrowFunction.prototype );

doSomething.prototype.foo = "bar";
console.log( doSomething.prototype );

let instance = new doSomething();
instance.prop = "some value"; 
console.log( instance );
instance.__proto__.bla = "bla";
console.log( instance.__proto__ );
console.log( instance.__proto__ === doSomething.prototype);
