# es001

La nostra scelta è quella di un progetto Gradle. Leggere [qui](https://docs.gradle.org/current/userguide/declaring_dependencies.html), purtroppo va aggiornato (Gradle 7 er superiori).
```groovy
edpendencies {
    testImplementation 'org.junit.jupiter:junit-jupiter-api:5.8.2'
    testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine:5.8.2'
    implementation('com.github.almasb:fxgl:11.17')
}
```
Dobbiamo inoltre installare JavaFX, [qui](https://openjfx.io/openjfx-docs/#gradle), ora un progetto esterno a Java pur sempre gratuito.
```groovy
plugins {
    id 'java'
    id 'org.openjfx.javafxplugin' version '0.0.10'
}
...
javafx {
    version = "17"
    modules = [ 'javafx.controls' ]
}
```
Questo primo esempio lo prendiamo da [qui](https://github.com/AlmasB/FXGL/blob/dev/fxgl-samples/src/main/java/basics/BasicGameSample.java).