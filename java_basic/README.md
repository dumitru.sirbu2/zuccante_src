# Java Basic

In questi sorgenti proponiamo una proposta di avvicinamento alla programmazione Java.

## Introduzione

I primi due programmi per il III anno di Informatica

[HelloWorld.java](./HelloWorld.java)  
[Somma.java](./Somma.java)

## terzo anno

Si prendano in considerazione gli esempi delle cartelle

[objects](./objects)

## quarto anno

Si prendano in considerazione gli esempi delle cartelle

[extending](./extending)
