import java.util.Scanner;
import java.util.InputMismatchException;
// import java.util.*;

public class SumScanner {
    
    public static void main (String[] args){
        
        Scanner sc = new Scanner(System.in);
        int num1 = 0;
        int num2 = 0;
        //primo addendo
        System.out.println("inserisci il primo addendo");
        try {
            num1 = sc.nextInt();
        } catch (InputMismatchException e){
            System.err.println("hai inserito male il primo addendo");
            // e.printStackTrace();
            System.exit(1);
        }
        // secondo addendo
        System.out.println("inserisci il secondo addendo");
        try {
            num2 = sc.nextInt();
        } catch (InputMismatchException e){
            System.err.println("hai inserito male il secondo addendo");
            System.exit(1);
        }
        int somma = num1 + num2;
        System.out.println(" ... la somma è");
        System.out.println(somma);
    }
}
