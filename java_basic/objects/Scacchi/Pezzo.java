import java.util.ArrayList;

public class Pezzo {

    private static ArrayList<Pezzo> PEZZI = Game.PEZZI;

    public final static int RE = 1;
    public final static int DONNA = 2; // REGINA
    public final static int TORRE = 3;
    public final static int ALFIERE = 4;
    public final static int CAVALLO = 5;
    public final static int PEDONE = 6;

    public final static int BIANCO = 10;
    public final static int NERO = 11;

    public int pezzo;
    public int colore;
    public int riga; // da 1 a 8
    public int colonna; // da 1 a 8

    public Pezzo(int pezzo, int colore) {
        this.pezzo = pezzo;
        this.colore = colore;
        riga = colonna = 0; // out of the game board
    }

    public void muovi(int riga, int colonna){
        // use switch
    }

    // move the rook (it: corvo = torre)
    public void muoviT(int riga, int colonna) throws Exception {
        // sono all'interno della scacchiera?
        if (!testScacchiera(riga, colonna))
            throw new Exception("movimento illegale");
        // posizionamento iniziale
        if (this.riga == 0 && this.colonna == 0) {
            if (testPosizione(riga, colonna))
                throw new Exception("posizione già occupata");
            else
               this.riga = riga;
               this.colonna = colonna;
               return;    
        }   
        // il movimento della torre
        if (this.colonna != colonna && this.riga != riga)
            throw new Exception("movimento illegale");
        // verifico presenza pezzi lungo il tragitto (2 if)
        // verifica ultima posizione (2 if nidificati: magno, mi muovo, c'è il pezzo amico)

    }

    
    private boolean testPosizione (int riga, int colonna) {
        for(Pezzo p : PEZZI) {
            if (riga == p.riga && colonna == p.colonna)
                return true;
        }
        return false;
    }

    // verifico che la posizione sia interna alla scacchiera: true OK
    private static boolean testScacchiera (int riga, int colonna) {
        if (riga >=1 && riga <= 8 && colonna >= 1 && colonna <= 8)
            return true;
        return false;
    }

    public String toString(){

        if(colore == BIANCO)
            return "T" + "(" + riga + "," + colonna + ")";
        else
            return "t" + "(" + riga + "," + colonna + ")";
    }




}