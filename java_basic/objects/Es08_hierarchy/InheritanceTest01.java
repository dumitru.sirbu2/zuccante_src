public class InheritanceTest01 {

    public static void main(String[] args) {

        SuperClass obj1 = new SubClass();
        SubClass obj2 = new SubClass();
        System.out.println(obj1.field);
        System.out.println(obj2.field);
        System.out.println(((SubClass)obj1).field);
        // System.out.println(obj1.getField());
        System.out.println(((SubClass)obj1).getField());
        System.out.println(obj2.getField());
        
        

    }

}

class SuperClass {

    public String field = "inside super";

}

class SubClass extends SuperClass {

    public String field = "inside sub";

    public String getField() {
        return super.field;
    }

}