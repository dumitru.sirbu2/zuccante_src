public class Pterox extends Monster implements Fighter {

    int flightLevel = 0;

    public Pterox(String n, int c, int p, int fl) {
        super(n, c);
        flightLevel = fl;
    }

    public Pterox(String n, int c, int p) {
        this(n, c, p, 100);
    }

    public void fly() {
        System.out.println(name + " is flying at " + flightLevel);
    }
    
    @Override
    public int fight(Monster enemy) {
        if(power > enemy.power && color != Color.BLUE) {
            enemy.power--;
            power++;
            return 1;
        } else {
            enemy.power++;
            power--;
            return -1;
        }
    }


}