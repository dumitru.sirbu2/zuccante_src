public class Tyros extends Monster implements Fighter {

    int velocity;

    public Tyros(String n, int c, int p, int vel) {
        super(n, c);
        velocity = vel;
    }

    public Tyros(String n, int c, int p) {
        super(n, c);
        velocity = 100;
    }

    @Override
    public int fight(Monster enemy) {
        return 1;
    }

    public void run() {
        System.out.println(name + " is running");
    }

}