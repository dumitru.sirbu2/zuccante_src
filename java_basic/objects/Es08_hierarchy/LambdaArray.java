public class ArrayLambda {
    public static void main(String[] args) {

        Algebra alg[] = new Algebra[] { 
            (a, b) -> a + b, 
            (a, b) -> a - b, 
            (a, b) -> a * b, 
            (a, b) -> a / b 
        };
        System.out.println("The addition of a and b is: " + alg[0].operate(30, 20));
        System.out.println("The subtraction of a and b is: " + alg[1].operate(30, 20));
        System.out.println("The multiplication of a and b is: " + alg[2].operate(30, 20));
        System.out.println("The division of a and b is: " + alg[3].operate(30, 20));
    }
}

@FunctionalInterface
interface Algebra {
    int operate(int a, int b);
}