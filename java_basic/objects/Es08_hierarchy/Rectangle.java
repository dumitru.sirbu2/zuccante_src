public class Rectangle extends Shape {

    float side1;
    float side2;

    public Rectangle(float side1, float side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    @Override
    public void draw(){
        System.out.println("drawed rectangle");
    }

    @Override
    public float getArea() {
        return side1*side2;
    }
}