public class Swap {
    
    int val;
    
    Swap(){}
    
    Swap(int v){
        val = v;
    }
    
    static void badSwap(int a, int b){
        int t = a;
        a = b;
        b = t;
    }
    
    static void badSwap(Swap a, Swap b){
        Swap t = new Swap();
        t = a;
        a = b;
        b = t;
    }
    
    static void goodSwap(Swap a, Swap b){
        Swap t = new Swap();
        t.val = a.val;
        a.val = b.val;
        b.val = t.val;
    }
    
    public static void main(String[] args){
        
        int num1, num2;
        num1 = 5;
        num2 = 6;
        System.out.println("num1 contiene " +  num1);
        System.out.println("num2 contiene " +  num2);
        System.out.println("bad swap");
        badSwap(num1, num2);
        System.out.println("num1 contiene " +  num1);
        System.out.println("num2 contiene " +  num2);
        System.out.println("*************************************");
        Swap swap1 = new Swap(5);
        Swap swap2 = new Swap(6);
        System.out.println("swap1.val è " +  swap1.val);
        System.out.println("swap2.val è " +  swap2.val);
        System.out.println("good swap:");
        goodSwap(swap1, swap2);
        System.out.println("swap1.val è " +  swap1.val);
        System.out.println("swap2.val è " +  swap2.val);
    }    
}
