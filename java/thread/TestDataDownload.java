import java.util.Date;
import java.util.Calendar;
import java.util.Random;

import java.util.concurrent.TimeUnit;

public class TestDataDownload {
    public static void main(String[] args) {
        DataDownloader ddl = new DataDownloader();
        Thread td = new Thread(ddl,"DataDownloaderThread");
        Thread tn = new NetworkConnection(td,"NetworkConnectionThread");
        tn.start();
        td.start();
    }
}


class DataDownloader implements Runnable {
    
    static Random rnd = new Random();
    
    @Override
    public void run() {
        Calendar cal = Calendar.getInstance();
        System.out.printf("Connecting: %s\n",
                           cal.getTime());
        try {
            int time = 2+rnd.nextInt(4);
            System.out.println("downloading for " + time + " seconds");
            TimeUnit.SECONDS.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("Download finished: %s\n", 
                           cal.getTime());
    }
}

class NetworkConnection extends Thread {
    
    private Thread td; // the downloader
    
    NetworkConnection(Thread td, String name){
        super(name);
        this.td = td;
    }
    
    @Override
    public void run() {
        Calendar cal = Calendar.getInstance();
        System.out.printf("Network connected: %s\n",
                           cal.getTime());
        try {
            td.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("Network not connected: %s\n", 
                           cal.getTime());
    }
}


