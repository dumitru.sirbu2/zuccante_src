package com.example.es008_relationship.repositories;

import com.example.es008_relationship.models.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Long> {}
