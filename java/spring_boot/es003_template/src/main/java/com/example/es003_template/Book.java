package com.example.es003_template;

import java.util.Random;

public class Book {

    private final static Random rnd = new Random();

    private int bookId;
    private String isbn;
    private String title;
    private String author;
    private Double price;

    public Book(int bookId) {
        this.bookId = bookId;
        isbn = Integer.toString(bookId + 6124);
        title = "title " + (bookId + rnd.nextInt(10));
        author = "author " + bookId;
        price = (double) (100 * (bookId + rnd.nextInt(10)));
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookId=" + bookId +
                ", isbn='" + isbn + '\'' +
                ", bookTitle='" + title + '\'' +
                ", author='" + author + '\'' +
                ", price=" + price +
                '}';
    }
}
