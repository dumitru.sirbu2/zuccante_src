package com.example.es003_template;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

@RestController
public class BookController {

    @Autowired
    private BookService service;

    @GetMapping("/books")
    public ModelAndView getBooks(Map<String, Object> model){
        List<Book> bookList = service.getBookRange(1,7);
        model.put("bookList", bookList);
        return new ModelAndView("books", model);
    }
}
