# es001 dependency injection

# moduli usati

- `Spring Web`
- `Spring Boot DevTools`

## component

Un *component*, vedi **[API](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/stereotype/Component.html)**
> *Such classes are considered as candidates for auto-detection when using annotation-based configuration and classpath scanning.*

## prima versione

Nella prima versione solo un *component* e senza usare l'*annotation* `@Qualifier` il cui scopo è individuare il *bean* che nominiamo poi nella versione
definitiva.

## versione definitiva

Usiamo `@Qualifier` per scegliere pewr nome il *bean* il cui nome di default è quello della classe con iniziale minuscola.

## aggiornamento automatico

Potremmo usare il pacchetto Node `nodemon`
```
nodemon --exec "./gradlew bootRun
```
altra possibilità, quella ufficiale, prevede l'uso del modulo `Spring Boot DevTools`. Su IDEA `..compiler -> buiold project automatically`