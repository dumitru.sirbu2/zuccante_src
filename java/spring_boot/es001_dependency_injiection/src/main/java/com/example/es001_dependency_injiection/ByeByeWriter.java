package com.example.es001_dependency_injiection;

import org.springframework.stereotype.Component;

@Component("bye")
public class ByeByeWriter implements TextWriter {

    private String msg = "Bye Bye";

    @Override
    public String writeText() {
        return msg;
    }
}
