# es000_bean

La classe base per un¶applicazione `SpringApplication`, **[API](https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/SpringApplication.html)**: il metodo `run(...)` ritorna un
`ApplicationContext` **[API](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/context/ApplicationContext.html)**.

## ApplicationContext

Dove oggetti chiamati ***bean*** vengono gestiti in modo automatico, vedi IOC e *dependency injection*. Elencando i *bean* vediamo `es000BeanApplication`.

## Component

Nelle **[API](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/stereotype/Component.html)** troviamo

> Such classes are considered as candidates for auto-detection when using annotation-based configuration and classpath scanning

in sostanza è in grado di individuare i nostri *bean*.

## Bean

I *bean* appaiono sotto varie specie, leggi *annotation* per quanto riguarda l'*annotation* `@Bean` essa, vedi **[API](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/context/annotation/Bean.html)** essa si riferisce ad un metodo.

> *ndicates that a method produces a bean to be managed by the Spring container*

Da noi i metodi *bean* (che usano l'omonima *annotation*) appaiono in una classe `@Configuration`, vedi [qui](https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/autoconfigure/SpringBootApplication.html).

## il nostro esempio

Nellamprima versione abbiamo lasciato vuota la classe `Address` ed abbiamo creato un metodo *bean* sulla classe di configurazione
```java
@SpringBootApplication
public class Es000BeanApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Es000BeanApplication.class, args);
		System.out.println("*************** BEANS *****************************");
		for(String name : context.getBeanDefinitionNames())
			System.out.println(name);
		System.out.println("*************** BEANS *****************************");
	}

	@Bean
	public String getName(){
		return "Andrea Morettin";
	}
}
```
e
```java
@Component
public class Address {

    private String location;
    private String number;
    private String ZIP;

    public Address(){
        location = "via dello SQuero Bello";
        number = "17";
        ZIP = "30172";
    }

    ...
}
```
l!oggetto *bean* viene creato automaticamente, non serve per il campo `address` di `Customer un metodo *bean* nella configurazione.