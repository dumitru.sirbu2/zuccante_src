# am013_authentication_jpa

L'esempio è tratto, con i soliti aggiustamenti, dall'esempio [qui](https://www.websparrow.org/spring/spring-boot-spring-security-with-jpa-authentication-and-mysql).

## creazione progetto

- Spring Web
- Spring Data JPA
- Spring Boot DevTools
- MariaDB Driver
- Security

## DB e model

```
CREATE DATABASE es013SB;
GRANT ALL ON es013SB.* to 'es013SB'@localhost IDENTIFIED BY 'zuccante@2021';
```

## gestione degli utenti

Andiamo ad esaminare le singole classi utilizzate.

## `@Userdetails`

[doc](https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/core/userdetails/UserDetails.html). 
> They simply store user information which is later encapsulated into Authentication objects.

Siamo tenuti ad implementare i seguenti metodi
- `public Collection<? extends GrantedAuthority> getAuthorities()`
- `public String getPassword()`.
- `public String getUsername()`.
- `public boolean isAccountNonExpired()`: per semplicità.
- `public boolean isAccountNonLocked()`: vedi sopra.
- `public boolean isCredentialsNonExpired()`: vedi sopra.
- `public boolean isEnabled()`: vedi sopra.

## `@UserDetailsService`

[doc](https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/core/userdetails/UserDetailsService.html)].
Il metodo che implementiamo
- `UserDetails loadUserByUsername(String username) throws UsernameNotFoundException` 
> locates the user based on the username.

## `User`

[doc](https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/core/userdetails/User.html) il *model*.
I **ruoli** vengono rappresentati come un unica `String` che poi viene splittata: abbiamo infatti in `UserDetails` il campo  
`List<GrantedAuthority> authorities`  
nel costruttore
```java
this.authorities = Arrays.stream(user.getRoles().split(","))
        .map(SimpleGrantedAuthority::new)
        .collect(Collectors.toList());
```
un utente può avere più ruoli! In [3] abbiamo un esempio più articolato. Tecnicamente esiste una differenza fra ***authority*** e ***role***: un ruolo si configura tramite una o più *authority*.
Abbiamo che `hasAuthority('ROLE_ADMIN')` è equivalente a `hasRole('ADMIN')`: in questo esempio non abbiamo usato il prefisso `_ROLE`; questo per associare ad un ruolo un *authority*. 
Per fare qualcosa di più complesso invitiamo ad esempio a vedere [4].

## `SimpleGrantAuthority`

[doc](https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/core/authority/SimpleGrantedAuthority.html) ci offre una sempliceimplementazione
dell'interfaccia `GrantAuthority` in questo caso data da una semplice stringa (vedi sopra).

## Configurazione

Usando l'*adapter*, [qui](https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/config/annotation/web/configuration/WebSecurityConfigurerAdapter.html) per la dcoumentazione,  
 `protected void configure​(AuthenticationManagerBuilder auth)` doc [qui](https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/config/annotation/web/configuration/WebSecurityConfigurerAdapter.html#configure(org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder))  
lo usiamo nel seguente modo
```java
@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(getPasswordEncoder());
    }
```
con `userDetailsService(userDetailsService)` configuriamo in modo custom,  
con `passwordEncioder` terminiamo il DAO.

In alternativa
```java
@Override
protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider());
        }

 @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(getPasswordEncoder());
        return authProvider;
    }
```
Possiamo eliminare il `formLogin()` esplicitamente con
```
.formLogin().disable();
```

## prove con curl

Grazie a `.httpBasic()` che, come visto in un precedente esempio ha solo funzione di richiesta possiamo procedere

**all**
```
curl localhost:8080/all
{"text":"Hello everyone"}
```
**user**
```
curl localhost:8080/user
{"timestamp":"2021-07-26T08:33:54.659+00:00","status":401,"error":"Unauthorized","message":"Unauthorized","path":"/user"
```
con autenticazione
```
curl -u 'user:user@2021' localhost:8080/user
{"text":"Welcome user"}
```

**admin**

```
curl -u 'admin:zuccante@2021' localhost:8080/admin
{"text":"Welcome admin"}
```

## altri materiali

[2] "Registration with Spring Security – Password Encoding" da Baeldung [qui](https://www.baeldung.com/spring-security-registration-password-encoding-bcrypt).  
[2] "Granted Authority Versus Role in Spring Security" by Baeldung [qui](https://www.baeldung.com/spring-security-granted-authority-vs-role).
[3] "Spring Boot Security Role-based Authorization Tutorial" di Nam Ha Minh[qui](https://www.codejava.net/frameworks/spring-boot/spring-boot-security-role-based-authorization-tutorial).  
[4] "Spring Security Roles and Permissions" in Java Development Journal [qui](https://www.javadevjournal.com/spring-security/spring-security-roles-and-permissions/)