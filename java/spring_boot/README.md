# Gli esempi

- **demo**: l'esempio zero con *live reloading* sia con *node* che con *dev tools*.
- **es001_rest**: il primo `REST` client.
- **es002_restfull**: un restfull completo con`GET`, `POST`, `PUT` e `DELETE`.
- **es004_crud_hibernate**: tratto da un esempio in rete.
- **es005_crud_hibernate**: tratto da un tutorial ufficiale: REST `json` e WB.



## riferimetni

[1] Fonte ufficiale ed imprescindibile di tutorial: [qui](https://spring.io/guides#topical-guides).  
[2] Un corso [qui](https://www.kindsonthegenius.com/spring-boot/spring-boot-entity-relationship/).
