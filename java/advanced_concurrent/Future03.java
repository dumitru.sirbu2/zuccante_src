import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class Future03 {

    public static void main(String[] args) throws Exception {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
    
        Future<String> dataReadFuture = executorService.submit(new DataReader());
        Future<String> dataProcessFuture = executorService.submit(new DataProcessor());
    
        System.out.println("Doing another task in anticipation of the results.");
        try {
            TimeUnit.SECONDS.sleep(1);   
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(dataReadFuture.get());
        System.out.println(dataProcessFuture.get());
        try {
            TimeUnit.SECONDS.sleep(2);   
        } catch (Exception e) {
            e.printStackTrace();
        }
        executorService.shutdown();
    }
    
    
}
