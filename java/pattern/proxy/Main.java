public class Main {

    public static void main(String[] args) {
        Video video = new ProxyVideo("film_test.mp4");
        video.display();
        System.out.println(" ... and again ... not loading from disk ...");
        video.display();
    }
    
}
