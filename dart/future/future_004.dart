import 'dart:math';

// "crivello di Eratostene" improved
Future<bool> isPrimeNumber(int number) async {
  if (number == 1) return false;
  if (number == 2) return true;

  double mysqrt = sqrt(number);
  // ceil: the least integer no smaller than
  int limit = mysqrt.ceil();

  for (int i = 2; i <= limit; ++i) {
    if (number % i == 0) return false;
  }

  return true;
}

void main() async {
  await Future.forEach(
      [10000, 97, 82, 81, 10, 9, 8, 5, 3],
      (int n) => isPrimeNumber(n)
          .then((x) => print("${n}${x ? " is" : " is not"} a prime number")));

  print('done!');
}
