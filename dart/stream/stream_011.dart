import 'dart:async';

main() {
  StreamController<int> streamController = StreamController.broadcast();
  Stream<int> stream =
      Stream<int>.periodic(Duration(seconds: 1), (value) => value + 1);
  stream = stream.take(8);
  streamController.addStream(stream);
  StreamSubscription sub1 = streamController.stream.listen((data) {
    print('recieved1: $data');
  });
  sub1.pause(Future.delayed(Duration(seconds: 3), () => print('fire')));
  // try to uncomment
  // sub1.cancel();
  streamController.stream.listen((data) {
    print('recieved2: $data');
  });
}
