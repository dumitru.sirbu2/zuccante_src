# es001_copy_txt a first build example

Tratto da [qui](https://dev.to/graphicbeacon/conquering-code-generation-in-dart-part-1-write-your-first-builder-4g9g) con lievi modifiche. Per un ripasso di pacchetti e librerie (i file di Dart) [qui](https://dart.dev/guides/libraries/create-library-packages). Il comando `pub` si trova assieme a `dart` ma non sempre nel `PATH` e quindi va aggiunto ... oppure si puuò usare `flutter pub get ... ` avendo `flutter` fra gli eseguibili nel `PATH`. 

## il pacchetto di riferimento

- `build` [qui](https://pub.dev/packages/build) ci permette di creare un `Builder` un modo per generare del codice.
- `build_config` [qui](https://pub.dev/packages/build_config) permette di creare un file di configurazione `build.yaml` un file di configurazione.
- `build_runner: ^2.1.7` [qui](https://pub.dev/packages/build_config).

## creazione del progetto

Un tempo vi **stagehand**, [qui](https://pub.dev/packages/stagehand), ora usiamo `dart create` [qui](https://dart.dev/tools/dart-create), un po' come `flutter create`
```
dart create -t console-full es001_copy_txt
```
per eseguire
```
cd es001_copy_txt
dart run
```

## step 1

Nella directory `src` creiamo la classe che ci permetterà di implementare la *business logic*: `CopyBuilder` che implementa appunto `Builder`, invitiamo a leggere la definizione [qui](https://pub.dev/documentation/build/latest/build/Builder-class.html)! Procediamo ad implementare i metodi mancanti
``` dart
@override
Map<String, List<String>> get buildExtensions => {
  '.txt': ['.copy.txt']
}
```
con cui gestiamo le estensioni: mappa fra sorgente e lista di destinazioni. Ed ora veniamo alla produzione vera e propria
``` dart
@override
Future<FutureOr<void>> build(BuildStep buildStep) async {
  AssetId inputId = buildStep.inputId;
  print("*** copy from ${inputId.uri.toString()}");
  AssetId copyAssetId = inputId.changeExtension('.copy.txt');
  print("*** copy to ${copyAssetId.uri..toString()}");
  String contents = await buildStep.readAsString(inputId);
  await buildStep.writeAsString(copyAssetId, '''
    ${DateTime.now()}
    =====================
    $contents
    ''');
}
```
`AssetId` [qui](https://pub.dev/documentation/build/latest/build/AssetId-class.html). `BuildStep` [qui](https://pub.dev/documentation/build/latest/build/BuildStep-class.html) è un passaggio della costruzione.

## step 2

nel file principale `es001_copy_txt.dart` dichiariamo la libreria `es001_copy_txt` ed il metodo factory `copyBuilder(...)`. Il file `build.yaml` lo richiede.

## step 3

Ora arriviamo al ducnque: il file `build.yaml`. Può essere utile un ripasso di `yaml` [qui](https://en.wikipedia.org/wiki/YAML): esso è un linguaggioche estende `json` (Node usa `json` Dart usa `yaml`). Il riferimento è [qui](https://pub.flutter-io.cn/packages/build_config).
```
builders: 
  copyBuilder: 
    import: 'package:es001_copy_txt/es001_copy_txt.dart' 
    builder_factories: ['copyBuilder'] 
    build_extensions: {'.txt': ['.copy.txt']} 
    build_to: 'source'
    auto_apply: 'root_package'
```
Si parte dalla documentazione [qui](https://pub.dev/packages/build_config#defining-builders-to-apply-to-dependents): il primo builder è `copyBuilder` (`builders` è una *map* in `yaml`).
- `import`: si riferisce alla libreraia che contiene il `Builder` (ecco perchè dobbiamo dichiarare la libreria per non lasciarla anonima), è obbligatorio.
- `builder_factories`: sono i *factory* (una lista).
- `build_extensions` indica come le estensioni vengono trsaformate: una *map* che traduce l'estensione nella lista di estensioni (vedi `copy_builder.dart`).
- `build_to` dove butto il file, nel nostro caso `src`.
- `auto_apply` dove applichiamo il tutto, qui noi scegliamo il pacchetto corrente.
E per i `builders` abbiamo finito, ora vediamo a chi applicare i `builders` andando a definire i `target`
> When a Builder should be applied to a subset of files in a package the package can be broken up into multiple 'targets'.

La sintassi dei *target*, [qui](https://pub.dev/packages/build_config#dividing-a-package-into-build-targets) per la documentazione, è `'$definingPackageName:$targetname'` essi sono fatti di *sources*, *dependencise* (passi che precedono) e *builder*
```
targets: 
  $default: 
    builders: 
      es001_copy_txt|copyBuilder: 
        generate_for: ['lib/*'] 
        enabled: True 
```
anche qui una *map* , saremmo potuti partire al posto di `$default` indicado sorgente e destinazione: `es001_copy_txt:es001_copy_txt`; quindi il generatore nel nostro pacchetto 
> To construct a key, you join the package and name with a |, so for instance the bar builder in the foo package would be referenced like this foo|bar

`es001_copy_txt|copyBuilder`.

Per il dettaglio [qui](https://github.com/dart-lang/build/blob/master/docs/build_yaml_format.md).

## set 4

Quindi proviamo, entra in gioco `build_runner`
```
dart pub run build_runner build
```

## poi ... per apporofondire

I video dell'autore sono interessanti ... sul suo canale YouTube c'è dell'altro!
