import 'package:analyzer/dart/element/element.dart';
import 'package:build/src/builder/build_step.dart';
import 'package:source_gen/source_gen.dart';
import '../my_annotation.dart';

class AnnotationGenerator extends GeneratorForAnnotation<MyAnnotation> {
  @override
  generateForAnnotatedElement(
      Element element, ConstantReader annotation, BuildStep buildStep) {
    String className = element.displayName;
    String path = buildStep.inputId.path;
    String prop = annotation.peek('prop')?.stringValue ?? '';
    return '''
// ${DateTime.now()}
// =====================
// classname: $className
// path: $path
// prop: $prop
''';
  }
}
